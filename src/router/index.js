//import vue router
import { createRouter, createWebHistory } from "vue-router";

//define a routes
const routes = [
  {
    path: "/",
    name: "index",
    component: () => import("../views/pages/index.vue"),
  },
  {
    path: "/genre/:id?",
    name: "pages.genre",
    component: () => import("../views/pages/genre.vue"),
  },
  {
    path: "/buku/:id?",
    name: "pages.katalog",
    component: () => import("../views/pages/katalog.vue"),
  },
  {
    path: "/cart",
    name: "pages.Cart",
    component: () => import("../views/pages/Cart.vue"),
  },
  {
    path: "/order",
    name: "pages.Order",
    component: () => import("../views/pages/Order.vue"),
  },
  {
    path: "/mypesanan",
    name: "pages.myOrder",
    component: () => import("../views/pages/MyOrder.vue"),
  },
  {
    path: "/login",
    component: () => import("../views/login.vue"),
  },
  {
    path: "/register",
    component: () => import("../views/register.vue"),
  },
  {
    path: "/invoice/:id?",
    component: () => import("../views/pages/Invoice.vue"),
  },
  {
    path: '/admin/show-buku',
    name: 'adminShowBuku',
    component: () => import("../views/pages/adminShowBuku.vue"),
    meta: { requiresAuth: true } // Jika rute memerlukan otentikasi, Anda dapat menambahkan metadata seperti ini
  },
  {
    path: '/admin/add-buku',
    name: 'adminAddBuku',
    component: () => import("../views/pages/adminAddBuku.vue"),
    meta: { requiresAuth: true } // Jika rute memerlukan otentikasi, Anda dapat menambahkan metadata seperti ini
  },
  {
    path: '/admin/edit-buku/:id',
    name: 'adminEditBuku',
    component: () => import("../views/pages/adminEditBuku.vue"),
    meta: { requiresAuth: true } // Jika rute memerlukan otentikasi, Anda dapat menambahkan metadata seperti ini
  },
  {
    path: '/admin/show-user',
    name: 'adminShowUser',
    component: () => import("../views/pages/adminShowUser.vue"),
    meta: { requiresAuth: true } // Jika rute memerlukan otentikasi, Anda dapat menambahkan metadata seperti ini
  },
  {
    path: '/admin/transaksi/:id',
    name: 'adminUserPesanan',
    component: () => import("../views/pages/adminUserPesanan.vue"),
    meta: { requiresAuth: true } // Jika rute memerlukan otentikasi, Anda dapat menambahkan metadata seperti ini
  },
  {
    path: "/login",
    component: () => import("../views/login.vue"),
  },
  {
    path: "/register",
    component: () => import("../views/register.vue"),
  },
];


//create router
const router = createRouter({
  history: createWebHistory(),
  routes, // <-- routes,
});


export default router;
