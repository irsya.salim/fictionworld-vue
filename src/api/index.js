// src/api/index.js
import axios from "axios";

const Api = axios.create({
  baseURL: "http://127.0.0.1:8000/api",
});

export const login = (email, password) => {
  return axios
    .post(`http://127.0.0.1:8000/api/login`, { email, password })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
};

export const register = (name, email, password) => {
  return axios
    .post(`http://127.0.0.1:8000/api/register`, { name, email, password })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      throw error;
    });
};

export const tambahBuku = async (formData) => {
  try {
    const response = await axios.post(
      `http://127.0.0.1:8000/api/buku`,
      formData,
      {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      }
    );
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const getGenre = async () => {
  try {
    const response = await axios.get(`${baseURL}/genre`);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export default Api;
